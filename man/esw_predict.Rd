% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/esw_predict.R
\name{esw_predict}
\alias{esw_predict}
\title{Predict ESW}
\usage{
esw_predict(newdat, model, pred_cat = NULL, ic = FALSE)
}
\arguments{
\item{newdat}{\code{data frame} including all variables included in the model except random effects}

\item{model}{\code{object of class eswmod} returned from \code{esw_run()} or \code{esw_run_all()}}

\item{pred_cat}{\code{character} group category used for esw prediction if the group variable is not included in \code{newdat}}

\item{ic}{\code{logical} whether columns with standard deviation and credible intervals of ESW should also be added}
}
\value{
data frame with new columns for predicted ESW
}
\description{
Add an esw column predicted from as eswmod model to a data frame
}
\examples{
data(sightings)
sightings <- sightings \%>\%
  dplyr::mutate(Year = lubridate::year(DateTime),
                Beaufort = sample(c(0:4), nrow(.), replace = TRUE))
form0 = "PerpDist ~  G(Species) + Beaufort"

a <- esw_run(1, all_mods = c(form0), dat = sightings, model_type = "Fus",
             run = list(nch = 3, nit = 1000, nburnin = 100, nthin = 1))

data(effort)
effort <- effort\%>\%
  sf::st_drop_geometry()\%>\%
  esw_predict(model = a$model,
              pred_cat = "ddel",
              ic = TRUE)

}
