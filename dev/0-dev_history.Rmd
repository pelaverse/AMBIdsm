---
title: "Development actions history"
output: html_document
editor_options: 
  chunk_output_type: console
---

All commands that to use when developing packages...

# First time just after creating the project

- Fill the following chunk to create the DESCRIPTION of your package

```{r description, eval=FALSE}
# Describe your package
fusen::fill_description(
  pkg = here::here(),overwrite = T,
  fields = list(
    Title = "Estimate ESW and Abundance",
    Description = "Prepare data and build distance sampling and DSM models to estimate abundance of species. Use Fusion effects to estimate esw. Use nimble for special and overdispersed DSM models.",
    `Authors@R` = c(
        person("Floriane", "Plard", email = "floriane.c.plard@gmail.com", role = c("aut", "cre")),
        person("Matthieu", "Authier", email = "matthieu.authier@univ-lr.fr", role = c("aut","ctb")),
        person("Mathieu", "Genu", email = "mathieu.genu@gmail.com", role = c("aut","ctb")),
        person("Auriane", "Virgili", email = "auriane.virgili@gmail.com", role = c("aut","ctb")),
        person("Rémi", "Pigeault", email = "remi.pigeault@gmail.com", role = c("aut","ctb"))
    )
  )
)
# Define License with use_*_license()
usethis::use_mit_license("Floriane Plard")
```

# Start using git

```{r, eval=FALSE}
usethis::use_git()
# Deal with classical files to ignore
usethis::git_vaccinate()
# Use main for primary branch
usethis::git_default_branch_rename()
```

# Start using data
```{r, eval=FALSE}
usethis::use_data_raw()
dir.create(here::here("inst"))

usethis::use_r("effort")
usethis::use_r("sightings")
usethis::use_r("trunc_dis")
usethis::use_r("dsm_data")
usethis::use_r("region")

```


# Set extra sources of documentation

```{r, eval=FALSE}
# Install a first time
remotes::install_local()
# README
usethis::use_readme_rmd()
# Code of Conduct
usethis::use_code_of_conduct("contact@fake.com")
# NEWS
usethis::use_news_md()
# new version of the package
usethis::use_version()
```

**From now, you will need to "inflate" your package at least once to be able to use the following commands. Let's go to your flat template, and come back here later if/when needed.**


# Package development tools
## Use once

```{r, eval=FALSE}
# Pipe
usethis::use_pipe()
utils::globalVariables("where")
# package-level documentation
usethis::use_package_doc()

# Set Continuous Integration
# _GitLab
gitlabr::use_gitlab_ci(type = "check-coverage-pkgdown")

#coverage
covr::report()

# Add new flat template
fusen::add_flat_template("add")
```

## Use everytime needed

```{r}
# Simulate package installation
pkgload::load_all()

# Generate documentation and deal with dependencies
attachment::att_amend_desc()

# Check the package
devtools::check(vignettes = T)
```

# Share the package

```{r}
# set and try pkgdown documentation website
usethis::use_pkgdown()
#for complete site
pkgdown::build_site()
#for some parts
pkgdown::build_home()
pkgdown::build_articles()
pkgdown:::build_reference()
pkgdown::build_news()


# si on veut ca en interne pour l'utilisateur final
pkgdown::build_home(override = list(destination = "inst/site"))
pkgdown::build_site(override = list(destination = "inst/site"))

# build the tar.gz with vignettes to share with others
devtools::build(vignettes = T)
```

