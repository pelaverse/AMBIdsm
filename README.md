
<!-- README.md is generated from README.Rmd. Please edit that file -->

# AMBIdsm

<!-- badges: start -->
<!-- badges: end -->

AMBIdsm is a package created within the project
[CetAMBICion](https://www.cetambicion-project.eu/). It proposes tools to
:

- Merge data from different surveys
- Build distance sampling models for several surveys and species and
  estimate ESW using fusion effects
- Build density surface models and visualize prediction maps

## Installation

You can install the development version of AMBIdsm like so:

``` r
install.packages("remotes")
remotes::install_gitlab(
  host = "https://gitlab.univ-lr.fr",
  repo = "pelaverse/AMBIdsm",
  build_vignettes = FALSE
)
```

Please set the arg. ‘build_vignettes’ to `TRUE` to have detailed
examples of how to use the different functions.

### Open help

``` r
path <- system.file("site", "index.html", package = "AMBIdsm")
browseURL(path)
```

## A quick overview

### Prepare your dataset

Two datasets are needed:

- Effort: a data set including TransectID and information about
  environmental and observation condition along transects
- Sightings: a data set including detections made along transects with
  their perpendicular distances.

See Vignette **Prepare Dataset** for more details about data formatting,
and about variables and columns that must be included in the data.

Toy examples can be downloaded from the package to test it

``` r
library(AMBIdsm)
library(ggplot2)
data(effort)
data(sightings)
```

##### Check your data:

``` r
effort <- effort %>%
  eff_check_columns()

check <-effort %>%
  eff_check_leg(crs = 4326)
check$mapfig

sightings<- sightings %>%
  sight_check_columns()
```

##### Linearize and segment effort data

``` r
effort <- effort %>%
  eff_linearize(variable_keep = c("Beaufort","SurveyID"), 
                crs = 4326
                ) %>%
  eff_segment(length = 10)
```

##### Add observation conditions to sightings

``` r
sightings <- sightings %>%
    sight_bindSegID(effort, 
    crs = 3035, 
    variable_join = 'Beaufort'
    )
```

### Build Distance Sampling Models

To estimate ESW according to species, survey and observation conditions,
you can run distance sampling models on your merge sightings directly.
With one formula, the function `esw_run_all` will build all possible
models using all variables given in formula as the most complex model
and simpler models using combinations of the variables. This way, you
can compare the fit and predictions of different models. The simplest
model includes all variables entered as group (`G()`, for model-based
clustering using fusion models) or random effects (`R()`). For more
details about this function and related ones, see Vignette **Estimate
ESW**.

`trunc_dis` should also be given. This data frame must includes a column
`Species` and a column `w` for truncation distances

##### Build models

``` r
data(trunc_dis)
sightings <- sightings %>%
  dplyr::mutate(PerpDist = PerpDist / 1000)
form0 = PerpDist ~  G(Species)*Beaufort + G(SurveyID)

MOD <- esw_run_all(form0, 
                   dat = sightings, 
                   trunc_dis = trunc_dis,
                   parallel = TRUE, 
                   distri = "hn",
                   run = list(nit = 10000, nburnin = 2000, nthin = 10, nch = 3)
                   )
MOD$tab_cat
MOD$tab_fus
```

##### Check fit and visualize

``` r
mod <- 1
out <- MOD$best_models_fus[[mod]] %>% 
    esw_plot(dat = sightings, 
    plotDir = NULL
    )

out$esw_plot # effective strip width
out$caterp   # caterpillar plot of estimated coefficient
out$summary  # text summary
out$group    # clustering
```

##### Predict ESW

You can use the function `esw_predict` to add ESW to your effort data
before running density surface models.

``` r

effort <- effort %>%
  esw_predict(model = MOD$best_models_fus[[mod]],
              pred_cat = "ddel",
              ic = TRUE
              )
```

### Build Distribution Surface Models

##### Dowload environmental variables

This package uses European services, the website of EMODnet and
COPERNICUS to download oceanic environmental variables. See Vignette
**Environmental_variables** for details.

##### Prepare prediction maps

``` r
data(study_area)
#find the directory where examples of environmental files are saved within the package
file = system.file("static_var_EMODnet.gpkg", package = 'AMBIdsm')
RastDir = dirname(file)

#create a directory to save grids
GridDir = paste0(getwd(),'\\grid')
dir.create(GridDir)

# group dynamic variables over years
env_groupyr(RastDir = RastDir,
            dynamic_var = "thetao", 
            seasons= 'spring',
            years = 2020:2021,
            RastDirgp = GridDir
            )

# create predictions grids
env_grid(study_area = study_area, 
         res = 0.1, 
         RastDir = RastDir , 
         create_grid = T,
         static_var = "bathymetry", 
         dynamic_var = "thetao",
         seasons= 'spring',
         overyears = TRUE,
         GridDir = GridDir
         )
list.files(GridDir)
```

##### Add environmental variables to your effort data

Here is an example with a small dataset (only two years of environmental
variables are saved with the package). To be able to use the whole
dataset, you will need to download dynamic environmental variables of
other years, first.

``` r
effort <- effort[1:29,] %>%
  sf::st_as_sf() %>%
  env_bind(
    RastDir = RastDir, 
    static_var = "bathymetry", 
    dynamic_var ="thetaogradient"
    )
```

##### Add animal count to your effort data

``` r
effort <- effort %>%
    eff_bindObs(sightings, vect_species = "ddel")
```

##### Build DSM Models

For more details about how to use these functions, see Vignette **DSM**.

``` r
data(dsm_data)

mod <- dsm_run_all(data = dsm_data$effort,
                   predictors = c("thetao", "bathymetry"),
                   Yname = 'ind.ddel', 
                   eshw = "esw_ddel", 
                   effort= "Effort"
                   )

mod$all_fits_binded
```

##### Select a model and check its fit to data

``` r

out  <- mod %>% 
  dsm_plot(data = dsm_data$effort, x = 1,
           plotDir = NULL
           )

out$summary       # text summary
out$check_plot    # some diagnostics
out$splines_plot  # relationship with covariates
```

##### Visualize predicted density maps

``` r
file = system.file("Prediction_grid_allVars_poly_2021_6.gpkg", package = 'AMBIdsm')

pred <- mod %>% 
  dsm_predict(predata = sf::read_sf(file), 
              crs = 4326
              )

pred$summary
pred$prediction_map
pred$confidence_map
```

##### Determine predictions that are extrapolations

``` r
extra <- mod %>% 
  dsm_extrapol(data = dsm_data$effort, 
               preddata = sf::read_sf(file)
               )

extra %>% 
  ggplot() + 
  geom_sf(aes(fill = extrapol_stacking))
```
